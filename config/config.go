package config

import (
	"gopkg.in/yaml.v3"
	"os"
	"strings"
)

type Database struct {
	DBAddr   string `yaml:"dbaddr"`
	DbPort   string `yaml:"dbport"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Dbname   string `yaml:"dbname"`
}

type Config struct {
	Kafka struct {
		Brokers []string `yaml:"brokers"`
	} `yaml:"kafka"`
	Grpc struct {
		Address  string `yaml:"address"`
		GrpcJson string `yaml:"grpcjson"`
	} `yaml:"grpc"`
	Database   Database `yaml:"database"`
	DBShard1   Database `yaml:"db-shard-1"`
	DBShard2   Database `yaml:"db-shard-2"`
	Prometheus struct {
		Booking   string `yaml:"booking"`
		Payment   string `yaml:"payment"`
		Promocode string `yaml:"promocode"`
		Orders    string `yaml:"orders"`
	} `yaml:"prometheus"`
	Redis struct {
		Address string `yaml:"address"`
	} `yaml:"redis"`
}

func parseConfig(fileBytes []byte) (*Config, error) {
	cf := Config{}
	err := yaml.Unmarshal(fileBytes, &cf)
	if err != nil {
		return nil, err
	}
	return &cf, nil
}

func ReadConfig(name string) (Config, error) {
	b, err := os.ReadFile(name)
	if err != nil {
		return Config{}, err
	}
	cfg, err := parseConfig(b)
	if err != nil {
		return Config{}, err
	}
	brokers, ok := os.LookupEnv("KAFKA_BROKERS")
	if ok {
		cfg.Kafka.Brokers = strings.Split(brokers, ";")
	}
	address, ok := os.LookupEnv("GRPC_ADDRESS")
	if ok {
		cfg.Grpc.Address = address
	}
	DBAddr, ok := os.LookupEnv("DB_HOST")
	if ok {
		cfg.Database.DBAddr = DBAddr
	}
	DbPort, ok := os.LookupEnv("DB_PORT")
	if ok {
		cfg.Database.DbPort = DbPort
	}
	redis, ok := os.LookupEnv("REDIS_ADDRESS")
	if ok {
		cfg.Redis.Address = redis
	}
	return *cfg, nil
}
