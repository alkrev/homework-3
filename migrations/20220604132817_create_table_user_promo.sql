-- +goose Up
-- +goose StatementBegin
CREATE TABLE user_promocodes (
    id SERIAL8 PRIMARY KEY NOT NULL,
    userid int8 NULL,
    pid int8 NOT NULL,
    used boolean NOT NULL,
    orderid int8 NOT NULL,
    CONSTRAINT user_promocodes_pid_fk FOREIGN KEY (pid) REFERENCES promocodes(id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT user_promocodes_pid_un UNIQUE (pid)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE user_promocodes DROP CONSTRAINT user_promocodes_pid_fk;
DROP TABLE user_promocodes;
-- +goose StatementEnd