-- +goose Up
-- +goose StatementBegin
CREATE TABLE payments (
     id SERIAL8 NOT NULL,
     userid int8 NULL,
     orderid int8 NOT NULL,
     status boolean NOT NULL,
     "date" timestamp NOT NULL
) partition by hash(id);
CREATE EXTENSION postgres_fdw;

GRANT USAGE ON FOREIGN DATA WRAPPER postgres_fdw to promocode;

CREATE SERVER shard_1 FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (dbname 'promocode', host 'db-shard-1', port '5432');
CREATE SERVER shard_2 FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (dbname 'promocode', host 'db-shard-2', port '5432');

CREATE USER MAPPING for promocode SERVER shard_1
    OPTIONS (user 'promocode', password 'promocode');
CREATE USER MAPPING for promocode SERVER shard_2
    OPTIONS (user 'promocode', password 'promocode');

CREATE FOREIGN TABLE payments_1 PARTITION OF payments
    FOR VALUES WITH (modulus 2, remainder 0)
    SERVER shard_1;
CREATE FOREIGN TABLE payments_2 PARTITION OF payments
    FOR VALUES WITH (modulus 2, remainder 1)
    SERVER shard_2;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP FOREIGN TABLE payments_1;

DROP USER MAPPING FOR promocode SERVER shard_1;

DROP SERVER shard_1;

REVOKE GRANT OPTION FOR USAGE ON FOREIGN DATA WRAPPER postgres_fdw FROM promocode;

DROP EXTENSION postgres_fdw;

DROP TABLE payments;
-- +goose StatementEnd
