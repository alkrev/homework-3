-- +goose Up
-- +goose StatementBegin
CREATE TABLE promocodes (
   id SERIAL8 PRIMARY KEY NOT NULL,
   code varchar NOT NULL,
   usage_count int4 NULL,
   used int4 NOT NULL,
   deadline timestamp NOT NULL,
   CONSTRAINT promocodes_code_un UNIQUE (code)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE promocodes;
-- +goose StatementEnd
