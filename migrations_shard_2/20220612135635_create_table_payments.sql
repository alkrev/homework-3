-- +goose Up
-- +goose StatementBegin
CREATE TABLE payments_2 (
id SERIAL8 NOT NULL,
userid int8 NULL,
orderid int8 NOT NULL,
status boolean NOT NULL,
"date" timestamp NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE payments_2;
-- +goose StatementEnd