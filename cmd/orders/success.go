package main

import (
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.ozon.dev/alkrev/homework-3/internal/order"
	"log"
	"time"
)

type SuccessHandler struct {
	Count   int
	C       GrpcClient
	Counter prometheus.Counter
}

func (s *SuccessHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (s *SuccessHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (s *SuccessHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var o order.Order
		err := json.Unmarshal(msg.Value, &o)
		if err != nil {
			log.Printf("successful payment message data %v: %v", string(msg.Value), err)
			continue
		}
		s.Count++
		s.Counter.Inc()
		_, err = s.C.CreatePayment(o.UserId, o.Id, true, time.Now())
		if err != nil {
			log.Printf("CreatePayment: %v", err)
		}
		session.MarkOffset(msg.Topic, msg.Partition, msg.Offset+1, "[successful]")
		session.Commit()
		log.Printf("[successful] ID: %v, Promocode: %v, Message: %q, UserId: %v", o.Id, o.PromoCodeID, o.Message, o.UserId)
	}
	return nil
}
