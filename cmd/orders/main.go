package main

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/prometheus"
	"log"
	"math/rand"
	"os/signal"
	"syscall"
	"time"

	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/grpc"

	prom "github.com/prometheus/client_golang/prometheus"
)

func D() int {
	return rand.Intn(10)
}

func GeneratePromo() string {
	return fmt.Sprintf("Promo %d%d%d%d", D(), D(), D(), D())
}

var (
	createdOrders = prom.NewCounter(prom.CounterOpts{
		Name: "orders_created_orders",
	})
	successPayments = prom.NewCounter(prom.CounterOpts{
		Name: "orders_success_payments",
	})
	unsuccessPayments = prom.NewCounter(prom.CounterOpts{
		Name: "orders_unsuccess_payments",
	})
)

func init() {
	// Metrics have to be registered to be exposed:
	prom.MustRegister(createdOrders)
	prom.MustRegister(successPayments)
	prom.MustRegister(unsuccessPayments)
}

func main() {
	log.Printf("starting")
	// rand init
	rand.Seed(time.Now().UnixNano())
	// Корректное завершение
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	defer stop()
	// Read settings
	c, err := config.ReadConfig("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	// Creating topics
	if err := createTopics(&c); err != nil {
		log.Fatalf("newSyncProducer: %v", err)
	}
	// gRPC клиент
	gc := grpc.NewClient(c.Grpc.Address)
	defer gc.Close()
	// kafka
	cfg := sarama.NewConfig()
	cfg.Producer.Return.Successes = true
	syncProducer, err := sarama.NewSyncProducer(c.Kafka.Brokers, cfg)
	if err != nil {
		log.Fatalf("newSyncProducer: %v", err)
	}
	// Creating orders
	created := creating(ctx, gc, syncProducer, createdOrders)
	// Read queue
	cfg.Consumer.Offsets.AutoCommit.Enable = false
	cfg.Consumer.Offsets.Initial = sarama.OffsetOldest
	sHandler, rHandler := readQueue(ctx, c, cfg, gc, successPayments, unsuccessPayments)

	// Prometheus
	srv := prometheus.Run(c.Prometheus.Orders, "/metrics")
	go func() {
		<-ctx.Done()
		if err := srv.Shutdown(ctx); err != nil {
			log.Printf("prometheus: %s", err)
		}
		stop()
	}()

	log.Printf("started")
	<-ctx.Done()
	log.Printf("created: %v, success payments: %v, unsuccess payments: %v", *created, sHandler.Count, rHandler.Count)
}
