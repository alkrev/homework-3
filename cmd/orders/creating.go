package main

import (
	"context"
	"github.com/Shopify/sarama"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
	"gitlab.ozon.dev/alkrev/homework-3/internal/order"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/producer"
	"log"
	"math/rand"
	"time"
)

func creating(ctx context.Context, client GrpcClient, syncProducer sarama.SyncProducer, counter prometheus.Counter) *int {
	// Creating orders in kafka
	var created int
	go func() {
	forLabel:
		for {
			promoCode := models.PromoCode{}
			apiKey := rand.Int63n(10) // UserId
			orderId := time.Now().UnixNano()

			if rand.Intn(10) < 5 {
				// Используем промокод

				// Ищем действующий промокод
				promoCodes, err := client.GetActivePromoCodeList()
				if err == nil && len(promoCodes) > 0 {
					// Нашли
					promoCode = promoCodes[rand.Intn(len(promoCodes))]
				} else {
					// Создаём новый промокод
					promoCode.Code = GeneratePromo()
					promoCode.UsageCount = int32(1 + rand.Intn(5))
					promoCode.Deadline = time.Now().Add(time.Second * 13 * 2)

					id, err := client.CreatePromoCode(apiKey, promoCode.Code, promoCode.UsageCount, promoCode.Deadline)
					if err != nil {
						// Не удалось создать промокод
						log.Printf("PromoCode creation failed: %v: %v", promoCode.Code, err)
						promoCode = models.PromoCode{}
					} else {
						promoCode.ID = id
						log.Printf("PromoCode created: %s", promoCode.String())
					}
				}
				if promoCode.ID != 0 {
					log.Printf("Use PromoCode %v for user '%d'", promoCode.String(), apiKey)
					if _, err := client.BindPromoCode(apiKey, promoCode.Code); err != nil {
						// Не удалось привязать промокод
						log.Printf("Binding PromoCode %v to user '%d' failed: %v", promoCode.String(), apiKey, err)
						promoCode = models.PromoCode{}
					} else {
						log.Printf("PromoCode %v has bound to user '%d'", promoCode.String(), apiKey)
					}
				}
			}

			o := order.Order{
				UserId:      apiKey,
				Id:          orderId,
				PromoCodeID: promoCode.ID,
			}
			err := producer.Send(syncProducer, incomeOrdersTopic, int(o.Id), o)
			if err != nil {
				log.Printf("%s: %s: %v", incomeOrdersTopic, o.String(), err)
			} else {
				created++
				counter.Inc()
				log.Printf("%s: created %s", incomeOrdersTopic, o.String())
			}

			select {
			case <-ctx.Done():
				{
					log.Printf("stopping create orders")
					break forLabel
				}
			default:
				{
					time.Sleep(time.Millisecond * 3000) // Создаём заказы с нужным периодом
				}
			}
		}
	}()

	return &created
}
