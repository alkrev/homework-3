package main

const (
	incomeOrdersTopic   = "income_orders"
	incomePaymentsTopic = "income_payments"

	resetConsumerGroupID = "ordersReset"
	resetOrdersTopic     = "reset_orders"

	successConsumerGroupID = "ordersSuccess"
	successPaymentsTopic   = "successful_payments"
)
