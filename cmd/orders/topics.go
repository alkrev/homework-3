package main

import (
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/topic"
	"log"
)

func createTopic(cfg *config.Config, name string, td *sarama.TopicDetail) error {
	topics, err := topic.ListTopics(cfg.Kafka.Brokers)
	if err != nil {
		return fmt.Errorf("error while getting list of topics: %w", err)
	}
	if _, ok := topics[name]; !ok {
		err = topic.Create(cfg.Kafka.Brokers, name, td)
		if err != nil {
			return fmt.Errorf("error while creating topic: %w", err)
		}
		log.Printf("created topic %v", name)
	}
	return nil
}

func createTopics(cfg *config.Config) error {
	if err := createTopic(cfg, incomeOrdersTopic, &sarama.TopicDetail{NumPartitions: 1, ReplicationFactor: 1}); err != nil {
		return err
	}
	if err := createTopic(cfg, incomePaymentsTopic, &sarama.TopicDetail{NumPartitions: 1, ReplicationFactor: 1}); err != nil {
		return err
	}
	if err := createTopic(cfg, resetOrdersTopic, &sarama.TopicDetail{NumPartitions: 1, ReplicationFactor: 1}); err != nil {
		return err
	}
	if err := createTopic(cfg, successPaymentsTopic, &sarama.TopicDetail{NumPartitions: 1, ReplicationFactor: 1}); err != nil {
		return err
	}
	return nil
}
