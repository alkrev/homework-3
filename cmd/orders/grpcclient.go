package main

import (
	"time"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

type GrpcClient interface {
	CreatePromoCode(apiKey int64, code string, usageCount int32, deadline time.Time) (int64, error)
	BindPromoCode(apiKey int64, code string) (int64, error)
	GetActivePromoCodeList() ([]models.PromoCode, error)
	CreatePayment(userId int64, orderId int64, status bool, date time.Time) (int64, error)
}
