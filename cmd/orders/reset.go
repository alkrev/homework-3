package main

import (
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.ozon.dev/alkrev/homework-3/internal/order"
	"log"
	"time"
)

type ResetHandler struct {
	Count   int
	C       GrpcClient
	Counter prometheus.Counter
}

func (r *ResetHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (r *ResetHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (r *ResetHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var o order.Order
		err := json.Unmarshal(msg.Value, &o)
		if err != nil {
			log.Printf("reset message data %v: %v", string(msg.Value), err)
			continue
		}
		r.Count++
		r.Counter.Inc()
		_, err = r.C.CreatePayment(o.UserId, o.Id, false, time.Now())
		if err != nil {
			log.Printf("CreatePayment: %v", err)
		}
		session.MarkOffset(msg.Topic, msg.Partition, msg.Offset+1, "[unsuccessful]")
		session.Commit()
		log.Printf("[unsuccessful] ID: %v, Promocode: %v, Message: %q, UserId: %v", o.Id, o.PromoCodeID, o.Message, o.UserId)
	}
	return nil
}
