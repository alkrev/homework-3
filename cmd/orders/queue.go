package main

import (
	"context"
	"github.com/Shopify/sarama"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/consumergroup"
	"log"
)

func readQueue(ctx context.Context, c config.Config, cfg *sarama.Config, client GrpcClient, successCounter prometheus.Counter, resetCounter prometheus.Counter) (*SuccessHandler, *ResetHandler) {
	// Reading successful payments
	sHandler := &SuccessHandler{
		C:       client,
		Counter: successCounter,
	}
	err := consumergroup.SaramaNewConsumerGroup(ctx, c.Kafka.Brokers, cfg, sHandler, successConsumerGroupID, successPaymentsTopic)
	if err != nil {
		log.Printf("successGroup consumer close error: %v", err)
	}
	// Reading unsuccessful payments
	rHandler := &ResetHandler{
		C:       client,
		Counter: resetCounter,
	}
	err = consumergroup.SaramaNewConsumerGroup(ctx, c.Kafka.Brokers, cfg, rHandler, resetConsumerGroupID, resetOrdersTopic)
	if err != nil {
		log.Printf("resetGroup consumer close error: %v", err)
	}
	return sHandler, rHandler
}
