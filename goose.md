# Goose

## Установка
    go install github.com/pressly/goose/v3/cmd/goose@latest

    go get github.com/pressly/goose/v3

## Команды
Из корня проекта:

    $(cd migrations && goose postgres "user=promocode password=promocode dbname=promocode sslmode=disable" status)
    
    $(cd migrations && goose create create_table_users sql)
    
    $(cd migrations && goose postgres "user=promocode password=promocode dbname=promocode sslmode=disable" up)
    $(cd migrations && goose postgres "user=promocode password=promocode dbname=promocode sslmode=disable" down)

    $(cd migrations && goose postgres "user=promocode password=promocode dbname=promocode sslmode=disable" reset)

#### Shard

    $(cd migrations_shard && goose create create_table_users sql)
    $(cd migrations_shard && goose postgres "user=promocode password=promocode dbname=promocode host=localhost port=5433 sslmode=disable" status)
    $(cd migrations_shard && goose postgres "user=promocode password=promocode dbname=promocode host=localhost port=5433 sslmode=disable" reset)
    $(cd migrations_shard && goose postgres "user=promocode password=promocode dbname=promocode host=localhost port=5433 sslmode=disable" up)
