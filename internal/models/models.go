package models

import (
	"fmt"
	"time"
)

type PromoCode struct {
	ID         int64
	Code       string
	UsageCount int32
	Used       int32
	Deadline   time.Time
}

func (p PromoCode) String() string {
	return fmt.Sprintf("[id:%d,promo:%q,count:%d/%d,deadline:%s]", p.ID, p.Code, p.Used, p.UsageCount, p.Deadline.Format("15:04:05"))
}

type UserPromoCode struct {
	ID      int64
	UserId  int64
	Pid     int64
	Used    bool
	OrderId int64
}

type Payment struct {
	ID      int64
	UserId  int64
	OrderId int64
	Status  bool
	Date    time.Time
}
