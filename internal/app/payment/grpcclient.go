package payment

type GrpcClient interface {
	SetPromoCodeUsed(apiKey int64, promoCodeId int64) error
}
