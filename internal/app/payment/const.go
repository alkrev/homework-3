package payment

const (
	incomeConsumerGroupID = "payment"
	incomePaymentsTopic   = "income_payments"

	successPaymentsTopic = "successful_payments"
	resetOrdersTopic     = "reset_orders"
)
