package payment

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/grpc"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/prometheus"
	"log"
	"math/rand"
	"os/signal"
	"syscall"
)

func Run() {
	rand.Seed(86)
	log.Println("starting")
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	defer stop()
	// Чтение настроек
	cfg, err := config.ReadConfig("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	// gRPC клиент
	gc := grpc.NewClient(cfg.Grpc.Address)
	defer gc.Close()
	// Работа
	err = readQueue(ctx, cfg, gc)
	if err != nil {
		log.Fatalf("%v", err)
	}
	// Prometheus
	srv := prometheus.Run(cfg.Prometheus.Payment, "/metrics")
	go func() {
		<-ctx.Done()
		if err := srv.Shutdown(ctx); err != nil {
			log.Printf("prometheus: %s", err)
		}
		stop()
	}()

	log.Println("started")
	<-ctx.Done()
	log.Println("stopped")
}
