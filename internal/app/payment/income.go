package payment

import (
	"encoding/json"
	"errors"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/alkrev/homework-3/internal/order"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/producer"
	"log"
)

type IncomeHandler struct {
	P sarama.SyncProducer
	C GrpcClient
}

func (i *IncomeHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (i *IncomeHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (i *IncomeHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var o order.Order
		err := json.Unmarshal(msg.Value, &o)
		if err != nil {
			log.Printf("json.Unmarshal, data %v: %v", string(msg.Value), err)
			continue
		}
		if o.PromoCodeID != 0 {
			var err = errors.New("no luck")
			if err != nil {
				r := order.Order{Id: o.Id, PromoCodeID: o.PromoCodeID, Message: "payment: a promo code validation error"}
				err := producer.Send(i.P, resetOrdersTopic, int(r.Id), r)
				if err != nil {
					log.Printf("send %q: %s: %v", resetOrdersTopic, r.String(), err)
				} else {
					session.MarkOffset(msg.Topic, msg.Partition, msg.Offset+1, "SetPromoCodeUsed failed")
					session.Commit()
					log.Printf("send %q: %s", resetOrdersTopic, r.String())
				}
			} else {
				o.Message = "payment: an order has paid"
				err := producer.Send(i.P, successPaymentsTopic, int(o.Id), o)
				if err != nil {
					log.Printf("send %q: %s: %v", successPaymentsTopic, o.String(), err)
				} else {
					session.MarkOffset(msg.Topic, msg.Partition, msg.Offset+1, "SetPromoCodeUsed successful")
					session.Commit()
					log.Printf("send %q: %s", successPaymentsTopic, o.String())
				}
			}
		} else {
			o.Message = "payment: an order has paid"
			err := producer.Send(i.P, successPaymentsTopic, int(o.Id), o)
			if err != nil {
				log.Printf("send %q: %s: %v", successPaymentsTopic, o.String(), err)
			} else {
				session.MarkOffset(msg.Topic, msg.Partition, msg.Offset+1, "Sending successPaymentsTopic successful")
				session.Commit()
				log.Printf("send %q: %s", successPaymentsTopic, o.String())
			}
		}
	}
	return nil
}
