package payment

import (
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/consumergroup"
)

func readQueue(ctx context.Context, c config.Config, client GrpcClient) error {
	cfg := sarama.NewConfig()
	cfg.Producer.Return.Successes = true
	cfg.Consumer.Offsets.AutoCommit.Enable = false
	cfg.Consumer.Offsets.Initial = sarama.OffsetOldest
	syncProducer, err := sarama.NewSyncProducer(c.Kafka.Brokers, cfg)
	if err != nil {
		return err
	}

	iHandler := &IncomeHandler{
		P: syncProducer,
		C: client,
	}
	err = consumergroup.SaramaNewConsumerGroup(ctx, c.Kafka.Brokers, cfg, iHandler, incomeConsumerGroupID, incomePaymentsTopic)
	if err != nil {
		return fmt.Errorf("resetGroup consumer close error: %w", err)
	}
	return nil
}
