package promocode

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/prometheus"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/redis"
	"gitlab.ozon.dev/alkrev/homework-3/internal/repository"
	"log"
	"os/signal"
	"sync"
	"syscall"
)

func Run() {
	log.Println("starting")
	// Корректное завершение
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	defer stop()
	// Чтение настроек
	cfg, err := config.ReadConfig("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	// Строка подключения
	connString := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", cfg.Database.User, cfg.Database.Password, cfg.Database.Dbname, cfg.Database.DBAddr, cfg.Database.DbPort)
	shardConnString1 := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", cfg.DBShard1.User, cfg.DBShard1.Password, cfg.DBShard1.Dbname, cfg.DBShard1.DBAddr, cfg.DBShard1.DbPort)
	shardConnString2 := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", cfg.DBShard2.User, cfg.DBShard2.Password, cfg.DBShard2.Dbname, cfg.DBShard2.DBAddr, cfg.DBShard2.DbPort)
	// Выполняем миграции
	makeMigrations(ctx, connString, shardConnString1, shardConnString2)
	// Пул соединений с базой
	pool, _ := pgxpool.Connect(ctx, connString)
	if err := pool.Ping(ctx); err != nil {
		log.Fatal("error pinging db: ", err)
	}
	// Репозиторий
	repo := repository.New(pool)
	// Кеш
	cache := redis.New(cfg.Redis.Address)
	// Ожидаем завершения горутин
	var wg sync.WaitGroup
	// GRPC
	wg.Add(1)
	grpcServer(&wg, ctx, stop, repo, cache, cfg)
	// Prometheus
	srv := prometheus.Run(cfg.Prometheus.Promocode, "/metrics")
	wg.Add(1)
	go func() {
		defer wg.Done()
		<-ctx.Done()
		if err := srv.Shutdown(ctx); err != nil {
			log.Printf("prometheus: %s", err)
		}
		stop()
	}()

	log.Println("started")
	wg.Wait()
	log.Println("stopped")
}
