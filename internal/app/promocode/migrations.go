package promocode

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3/pkg/migrations"
	"log"
	"time"
)

const (
	migrationsFolder       = "migrations"
	migrationsShard1Folder = "migrations_shard_1"
	migrationsShard2Folder = "migrations_shard_2"
	retryCount             = 6
)

func makeMigration(ctx context.Context, connString string, folder string, retries int) {
	n := retries
	for i := 0; ; i++ {
		if err := migrations.Up(connString, folder); err != nil {
			if i < n {
				log.Println(err)
				log.Printf("migrations.Up %q: wait a few seconds (%d/%d)\n", folder, i+1, n)
			} else {
				log.Fatal(err)
			}
			for j := 0; j < 5; j++ {
				select {
				case <-ctx.Done():
					log.Fatal(ctx.Err())
				default:
					time.Sleep(time.Second) // Здесь нужно, ждём пока БД инициализируется
				}
			}
		} else {
			break
		}
	}
}
func makeMigrations(ctx context.Context, connString string, shardConnString1 string, shardConnString2 string) {
	makeMigration(ctx, shardConnString1, migrationsShard1Folder, retryCount)
	makeMigration(ctx, shardConnString2, migrationsShard2Folder, retryCount)
	makeMigration(ctx, connString, migrationsFolder, retryCount)
}
