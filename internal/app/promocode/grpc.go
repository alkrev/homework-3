package promocode

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/grpc/server"
	"log"
	"sync"
)

func grpcServer(wg *sync.WaitGroup, ctx context.Context, cancel context.CancelFunc, repo server.Repository, cache server.Cache, cfg config.Config) {
	go func() {
		defer wg.Done()
		if err := server.Run(ctx, repo, cache, cfg); err != nil {
			log.Printf("grpc: %s\n", err)
		} else {
			log.Printf("grpc: stopped")
		}
		cancel()
	}()
}
