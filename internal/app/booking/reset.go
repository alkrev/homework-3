package booking

import (
	"encoding/json"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/alkrev/homework-3/internal/order"
	"log"
)

type ResetHandler struct {
	P sarama.SyncProducer
	C GrpcClient
}

func (r *ResetHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (r *ResetHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (r *ResetHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var o order.Order
		err := json.Unmarshal(msg.Value, &o)
		if err != nil {
			log.Printf("json.Unmarshal, data %v: %v", string(msg.Value), err)
			continue
		}
		if err := r.C.ResetPromoCodeApply(o.UserId, o.PromoCodeID); err != nil {
			log.Printf("%s: %v", "ResetPromoCodeApply", err)
		} else {
			session.MarkOffset(msg.Topic, msg.Partition, msg.Offset+1, "ResetPromoCode successful")
			session.Commit()
			log.Printf("%s %s: success", "ResetPromoCodeApply", o.String())
		}
	}
	return nil
}
