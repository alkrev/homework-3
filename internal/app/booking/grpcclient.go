package booking

type GrpcClient interface {
	ApplyPromoCode(apiKey int64, orderId int64, promoCodeId int64) error
	ResetPromoCodeApply(apiKey int64, promoCodeId int64) error
}
