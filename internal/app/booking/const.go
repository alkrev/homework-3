package booking

const (
	incomeConsumerGroupID = "booking"
	incomeOrdersTopic     = "income_orders"

	incomePaymentsTopic = "income_payments"

	resetConsumerGroupID = "bookingReset"
	resetOrdersTopic     = "reset_orders"
)
