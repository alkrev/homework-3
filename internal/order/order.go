package order

import "fmt"

type Order struct {
	Id          int64  `json:"id"`
	UserId      int64  `json:"user_id"`
	PromoCodeID int64  `json:"promo_code_id"`
	Message     string `json:"message"`
}

func (o Order) String() string {
	return fmt.Sprintf("[%d,%v,%v,%q]", o.Id, o.PromoCodeID, o.UserId, o.Message)
}
