package server

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
	pb "gitlab.ozon.dev/alkrev/homework-3/pkg/api"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	PromoCodeKey = "pc"
)

func getCodeFromJSON(str string) (models.PromoCode, error) {
	var promoCode models.PromoCode
	if err := json.Unmarshal([]byte(str), &promoCode); err != nil {
		return models.PromoCode{}, err
	}
	return promoCode, nil
}

func (ts tserver) getPromoCodeFromCache(ctx context.Context, id int64) (models.PromoCode, error) {
	promoCodeJson, err := ts.cache.Get(ctx, fmt.Sprintf("%s:%d", PromoCodeKey, id))
	if err != nil {
		return models.PromoCode{}, err
	}
	return getCodeFromJSON(promoCodeJson)
}

func ConvertPromoCodeToPbPromoCode(p models.PromoCode) *pb.PromoCode {
	return &pb.PromoCode{
		Id:         p.ID,
		Code:       p.Code,
		UsageCount: p.UsageCount,
		Used:       p.Used,
		Deadline:   timestamppb.New(p.Deadline),
	}
}

func (ts tserver) setPromoCodeToCache(ctx context.Context, promoCode models.PromoCode) error {
	promoCodeByte, err := json.Marshal(promoCode)
	if err != nil {
		return err
	}
	return ts.cache.Set(ctx, fmt.Sprintf("%s:%d", PromoCodeKey, promoCode.ID), string(promoCodeByte))
}

func (ts tserver) invalidatePromoCodeInCache(ctx context.Context, id int64) error {
	return ts.cache.Del(ctx, fmt.Sprintf("%s:%d", PromoCodeKey, id))
}

func (ts tserver) ReadPromoCode(ctx context.Context, req *pb.ReadPromoCodeRequest) (*pb.ReadPromoCodeResponse, error) {
	promoCode, err := ts.getPromoCodeFromCache(ctx, req.Id)
	if err == nil {
		return &pb.ReadPromoCodeResponse{PromoCode: ConvertPromoCodeToPbPromoCode(promoCode)}, nil
	}
	promoCode, err = ts.repo.ReadPromoCode(ctx, req.Id)
	if err != nil {
		return nil, err
	}
	_ = ts.setPromoCodeToCache(ctx, promoCode)
	return &pb.ReadPromoCodeResponse{PromoCode: ConvertPromoCodeToPbPromoCode(promoCode)}, nil
}
