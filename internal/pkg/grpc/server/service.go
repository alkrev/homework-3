package server

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
	pb "gitlab.ozon.dev/alkrev/homework-3/pkg/api"
)

type tserver struct {
	repo  Repository
	cache Cache
	cfg   config.Config
	pb.UnimplementedPromoCodeServiceServer
}

func (ts tserver) CreatePromoCode(ctx context.Context, req *pb.CreatePromoCodeRequest) (*pb.CreatePromoCodeResponse, error) {
	if err := req.PromoCode.Deadline.CheckValid(); err != nil {
		return nil, err
	}
	id, err := ts.repo.CreatePromoCode(ctx, models.PromoCode{Code: req.PromoCode.Code, UsageCount: req.PromoCode.UsageCount, Used: 0, Deadline: req.PromoCode.Deadline.AsTime()})
	if err != nil {
		return nil, err
	}
	var UserID = pb.CreatePromoCodeResponse{Id: id}
	_ = ts.invalidateActivePromoCodeListInCache(ctx)
	return &UserID, nil
}

func (ts tserver) UpdatePromoCode(ctx context.Context, req *pb.UpdatePromoCodeRequest) (*empty.Empty, error) {
	if err := req.PromoCode.Deadline.CheckValid(); err != nil {
		return nil, err
	}
	err := ts.repo.UpdatePromoCode(ctx, models.PromoCode{ID: req.PromoCode.Id, Code: req.PromoCode.Code, UsageCount: req.PromoCode.UsageCount, Used: req.PromoCode.Used, Deadline: req.PromoCode.Deadline.AsTime()})
	_ = ts.invalidateActivePromoCodeListInCache(ctx)
	_ = ts.invalidatePromoCodeInCache(ctx, req.PromoCode.Id)
	return &empty.Empty{}, err
}
func (ts tserver) DeletePromoCode(ctx context.Context, req *pb.DeletePromoCodeRequest) (*empty.Empty, error) {
	err := ts.repo.DeletePromoCode(ctx, req.Id)
	_ = ts.invalidateActivePromoCodeListInCache(ctx)
	_ = ts.invalidatePromoCodeInCache(ctx, req.Id)
	return &empty.Empty{}, err
}

func (ts tserver) BindPromoCode(ctx context.Context, req *pb.BindPromoCodeRequest) (*pb.BindPromoCodeResponse, error) {
	promoCode, err := ts.repo.GetActivePromoCodeByCode(ctx, req.Code)
	if err != nil {
		return nil, err
	}
	id, err := ts.repo.CreateUserPromoCode(ctx, models.UserPromoCode{UserId: req.ApiKey, Pid: promoCode.ID, Used: false})
	if err != nil {
		return nil, err
	}
	_ = ts.repo.IncPromoCodeUsage(ctx, promoCode.ID)
	_ = ts.invalidateActivePromoCodeListInCache(ctx)
	_ = ts.invalidatePromoCodeInCache(ctx, promoCode.ID)
	var ID = pb.BindPromoCodeResponse{Id: id}
	return &ID, nil
}
func (ts tserver) ApplyPromoCode(ctx context.Context, req *pb.ApplyPromoCodeRequest) (*empty.Empty, error) {
	if err := ts.repo.SetUserPromoCodeOrderId(ctx, models.UserPromoCode{
		UserId:  req.ApiKey,
		Pid:     req.Id,
		OrderId: req.OrderId,
	}); err != nil {
		return nil, err
	} else {
		return &empty.Empty{}, nil
	}
}
func (ts tserver) SetPromoCodeUsed(ctx context.Context, req *pb.SetPromoCodeRequest) (*empty.Empty, error) {
	if err := ts.repo.SetUserPromoCodeUsed(ctx, models.UserPromoCode{
		UserId: req.ApiKey,
		Pid:    req.Id,
	}); err != nil {
		return nil, err
	} else {
		return &empty.Empty{}, nil
	}
}

func (ts tserver) CreatePayment(ctx context.Context, req *pb.CreatePaymentRequest) (*pb.CreatePaymentResponse, error) {
	id, err := ts.repo.CreatePayment(ctx, models.Payment{UserId: req.Payment.UserId, OrderId: req.Payment.OrderId, Status: req.Payment.Status, Date: req.Payment.Date.AsTime()})
	if err != nil {
		return nil, err
	}
	var UserID = pb.CreatePaymentResponse{Id: id}
	return &UserID, nil
}

func New(repo Repository, cache Cache, cfg config.Config) *tserver {
	return &tserver{repo: repo, cache: cache, cfg: cfg}
}
