package server

import (
	"context"
	"encoding/json"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
	pb "gitlab.ozon.dev/alkrev/homework-3/pkg/api"
)

const (
	PromoCodeListKey = "promoList"
)

func getCodeListFromJSON(str string) ([]models.PromoCode, error) {
	var promoCodes []models.PromoCode
	if err := json.Unmarshal([]byte(str), &promoCodes); err != nil {
		return nil, err
	}
	return promoCodes, nil
}

func (ts tserver) getActivePromoCodeListFromCache(ctx context.Context) ([]models.PromoCode, error) {
	promoCodesJson, err := ts.cache.Get(ctx, PromoCodeListKey)
	if err != nil {
		return nil, err
	}
	return getCodeListFromJSON(promoCodesJson)
}

func convertPromoCodeListToPointers(promoCodes []models.PromoCode) []*pb.PromoCode {
	var list = make([]*pb.PromoCode, 0, len(promoCodes))
	for _, p := range promoCodes {
		list = append(list, ConvertPromoCodeToPbPromoCode(p))
	}
	return list
}

func (ts tserver) setActivePromoCodeListToCache(ctx context.Context, promoCodes []models.PromoCode) error {
	promoCodesByte, err := json.Marshal(promoCodes)
	if err != nil {
		return err
	}
	return ts.cache.Set(ctx, PromoCodeListKey, string(promoCodesByte))
}

func (ts tserver) invalidateActivePromoCodeListInCache(ctx context.Context) error {
	return ts.cache.Del(ctx, PromoCodeListKey)
}

func (ts tserver) GetActivePromoCodeList(ctx context.Context, _ *empty.Empty) (*pb.ActivePromoCodeListResponse, error) {
	promoCodes, err := ts.getActivePromoCodeListFromCache(ctx)
	if err == nil {
		return &pb.ActivePromoCodeListResponse{List: convertPromoCodeListToPointers(promoCodes)}, nil
	}
	promoCodes, err = ts.repo.SelectActivePromoCodes(ctx)
	if err != nil {
		return nil, err
	}
	_ = ts.setActivePromoCodeListToCache(ctx, promoCodes)
	return &pb.ActivePromoCodeListResponse{List: convertPromoCodeListToPointers(promoCodes)}, nil
}
