package server

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

type Repository interface {
	CreatePromoCode(ctx context.Context, p models.PromoCode) (ID int64, err error)
	ReadPromoCode(ctx context.Context, ID int64) (p models.PromoCode, err error)
	UpdatePromoCode(ctx context.Context, p models.PromoCode) (err error)
	DeletePromoCode(ctx context.Context, ID int64) (err error)

	GetActivePromoCodeByCode(ctx context.Context, code string) (p models.PromoCode, err error)
	IncPromoCodeUsage(ctx context.Context, ID int64) (err error)
	SetUserPromoCodeOrderId(ctx context.Context, p models.UserPromoCode) (err error)
	SetUserPromoCodeUsed(ctx context.Context, p models.UserPromoCode) (err error)

	CreateUserPromoCode(ctx context.Context, up models.UserPromoCode) (ID int64, err error)

	SelectActivePromoCodes(ctx context.Context) (promoCodes []models.PromoCode, err error)

	CreatePayment(ctx context.Context, p models.Payment) (int64, error)
}
