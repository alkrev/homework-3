package server

import (
	"context"
	"flag"
	"fmt"
	"gitlab.ozon.dev/alkrev/homework-3/internal/mw"

	"log"
	"net"
	"net/http"

	grpcPrometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.ozon.dev/alkrev/homework-3/config"
	pb "gitlab.ozon.dev/alkrev/homework-3/pkg/api"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func Run(ctx context.Context, repo Repository, cache Cache, cfg config.Config) error {
	flag.Parse()
	apiServer := New(repo, cache, cfg)
	lis, err := net.Listen("tcp", cfg.Grpc.Address)
	if err != nil {
		return fmt.Errorf("failed to listen: %w\n", err)
	}
	grpcPrometheus.EnableHandlingTimeHistogram()
	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(mw.LogInterceptor, grpcPrometheus.UnaryServerInterceptor))
	pb.RegisterPromoCodeServiceServer(grpcServer, apiServer)
	grpcPrometheus.Register(grpcServer)

	mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{OrigName: true, EmitDefaults: true}))
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(50000000)),
	}

	srv := http.Server{Addr: cfg.Grpc.GrpcJson, Handler: mux}

	var group errgroup.Group

	group.Go(func() error {
		return grpcServer.Serve(lis)
	})

	group.Go(func() error {
		<-ctx.Done()
		grpcServer.GracefulStop()
		if err := srv.Shutdown(ctx); err != nil {
			log.Printf("json+grpc: %v\n", err)
		}
		return ctx.Err()
	})

	group.Go(func() error {
		return pb.RegisterPromoCodeServiceHandlerFromEndpoint(ctx, mux, cfg.Grpc.Address, opts)
	})

	group.Go(func() error {
		return srv.ListenAndServe()
	})

	return group.Wait()
}
