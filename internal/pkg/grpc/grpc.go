package grpc

import (
	"gitlab.ozon.dev/alkrev/homework-3/internal/pkg/grpc/client"
)

func NewClient(address string) *client.GrpcClient {
	return client.New(address)
}
