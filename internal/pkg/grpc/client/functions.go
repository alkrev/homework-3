package client

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"time"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
	pb "gitlab.ozon.dev/alkrev/homework-3/pkg/api"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (c *GrpcClient) CreatePromoCode(apiKey int64, code string, usageCount int32, deadline time.Time) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := c.client.CreatePromoCode(ctx, &pb.CreatePromoCodeRequest{
		ApiKey: apiKey,
		PromoCode: &pb.PromoCode{
			Code:       code,
			UsageCount: usageCount,
			Deadline:   timestamppb.New(deadline),
		},
	})
	if err != nil {
		return 0, err
	}
	return res.Id, nil
}

func (c *GrpcClient) getPromoCode(apiKey int64, promoCodeId int64) (models.PromoCode, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := c.client.ReadPromoCode(ctx, &pb.ReadPromoCodeRequest{
		ApiKey: apiKey,
		Id:     promoCodeId,
	})
	if err != nil {
		return models.PromoCode{}, err
	}
	if err := res.PromoCode.Deadline.CheckValid(); err != nil {
		return models.PromoCode{}, err
	}
	return models.PromoCode{
		ID:         res.PromoCode.Id,
		Code:       res.PromoCode.Code,
		UsageCount: res.PromoCode.UsageCount,
		Used:       res.PromoCode.Used,
		Deadline:   res.PromoCode.Deadline.AsTime(),
	}, nil
}

func (c *GrpcClient) BindPromoCode(apiKey int64, code string) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := c.client.BindPromoCode(ctx, &pb.BindPromoCodeRequest{
		ApiKey: apiKey,
		Code:   code,
	})
	if err != nil {
		return 0, err
	}
	return res.Id, nil
}

func (c *GrpcClient) ApplyPromoCode(apiKey int64, orderId int64, promoCodeId int64) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err := c.client.ApplyPromoCode(ctx, &pb.ApplyPromoCodeRequest{
		ApiKey:  apiKey,
		Id:      promoCodeId,
		OrderId: orderId,
	})
	return err
}
func (c *GrpcClient) ResetPromoCodeApply(apiKey int64, promoCodeId int64) error {
	return c.ApplyPromoCode(apiKey, -1, promoCodeId)
}
func (c *GrpcClient) SetPromoCodeUsed(apiKey int64, promoCodeId int64) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err := c.client.SetPromoCodeUsed(ctx, &pb.SetPromoCodeRequest{
		ApiKey: apiKey,
		Id:     promoCodeId,
	})
	return err
}

func (c *GrpcClient) GetActivePromoCodeList() ([]models.PromoCode, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := c.client.GetActivePromoCodeList(ctx, &empty.Empty{})
	if err != nil {
		return nil, err
	}
	promoCodes := []models.PromoCode{}
	for _, pbPromoCode := range res.List {
		if err := pbPromoCode.Deadline.CheckValid(); err != nil {
			return nil, err
		}
		p := models.PromoCode{
			ID:         pbPromoCode.Id,
			Code:       pbPromoCode.Code,
			UsageCount: pbPromoCode.UsageCount,
			Used:       pbPromoCode.Used,
			Deadline:   pbPromoCode.Deadline.AsTime(),
		}
		promoCodes = append(promoCodes, p)
	}
	return promoCodes, nil
}

func (c *GrpcClient) CreatePayment(userId int64, orderId int64, status bool, date time.Time) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := c.client.CreatePayment(ctx, &pb.CreatePaymentRequest{
		Payment: &pb.Payment{
			UserId:  userId,
			OrderId: orderId,
			Status:  status,
			Date:    timestamppb.New(date),
		},
	})
	if err != nil {
		return 0, err
	}
	return res.Id, nil
}
