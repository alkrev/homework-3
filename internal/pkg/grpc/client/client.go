package client

import (
	"flag"
	grpcPrometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	pb "gitlab.ozon.dev/alkrev/homework-3/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"sync"
)

type GrpcClient struct {
	client pb.PromoCodeServiceClient
	conn   *grpc.ClientConn
}

var (
	gc   GrpcClient
	once sync.Once
)

func New(address string) *GrpcClient {
	once.Do(func() {
		flag.Parse()
		var opts []grpc.DialOption
		grpcPrometheus.EnableHandlingTimeHistogram()
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithUnaryInterceptor(grpcPrometheus.UnaryClientInterceptor))
		var err error
		gc.conn, err = grpc.Dial(address, opts...)
		if err != nil {
			log.Fatalf("fail to dial: %v", err)
		}
		gc.client = pb.NewPromoCodeServiceClient(gc.conn)
	})
	return &gc
}

func (c *GrpcClient) Close() {
	if c.conn != nil {
		_ = c.conn.Close()
		c.conn = nil
	}
}
