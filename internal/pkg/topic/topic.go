package topic

import (
	"fmt"
	"github.com/Shopify/sarama"
)

func Create(brokers []string, name string, td *sarama.TopicDetail) error {
	cfg := sarama.NewConfig()
	admin, err := sarama.NewClusterAdmin(brokers, cfg)
	if err != nil {
		return fmt.Errorf("error while creating cluster admin: %w", err)
	}
	defer func() { _ = admin.Close() }()
	err = admin.CreateTopic(name, td, false)
	if err != nil {
		return fmt.Errorf("error while creating topic: %w", err)
	}
	return err
}

func ListTopics(brokers []string) (map[string]sarama.TopicDetail, error) {
	cfg := sarama.NewConfig()
	admin, err := sarama.NewClusterAdmin(brokers, cfg)
	if err != nil {
		return nil, fmt.Errorf("error while creating cluster admin: %w", err)
	}
	defer func() { _ = admin.Close() }()
	topics, err := admin.ListTopics()
	if err != nil {
		return nil, fmt.Errorf("error while getting list of topics: %w", err)
	}
	return topics, nil
}
