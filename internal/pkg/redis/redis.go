package redis

import (
	"context"
	"fmt"
	re "github.com/go-redis/redis/v8"
	"time"
)

type Redis struct {
	client *re.Client
}

func New(address string) *Redis {
	return &Redis{client: re.NewClient(&re.Options{
		Addr: address,
	})}
}

func (r Redis) Set(ctx context.Context, key string, val string) error {
	return r.client.Set(ctx, key, val, time.Minute).Err()
}

func (r Redis) Get(ctx context.Context, key string) (string, error) {
	val, err := r.client.Get(ctx, key).Result()
	if err == re.Nil {
		return "", fmt.Errorf("%v does not exist, redis: %w", key, err)
	}
	if err != nil {
		return "", fmt.Errorf("redis: %w", err)
	}
	return val, nil
}

func (r Redis) Del(ctx context.Context, key string) error {
	return r.client.Del(ctx, key).Err()
}
