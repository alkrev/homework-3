package consumergroup

import (
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"log"
)

func SaramaNewConsumerGroup(ctx context.Context, brokers []string, cfg *sarama.Config, handler sarama.ConsumerGroupHandler, consumerGroupID string, topic string) error {
	consumerGroup, err := sarama.NewConsumerGroup(brokers, consumerGroupID, cfg)
	if err != nil {
		return fmt.Errorf("NewConsumerGroup: %w", err)
	}
	go func() {
		defer func() {
			if err := consumerGroup.Close(); err != nil {
				log.Printf("%s consumer close error: %v", topic, err)
			}
		}()
	labels:
		for {
			err := consumerGroup.Consume(ctx, []string{topic}, handler)
			if err != nil {
				log.Printf("%s consumer error: %v", topic, err)
			}
			select {
			case <-ctx.Done():
				break labels
			default:
			}
		}
	}()
	return nil
}
