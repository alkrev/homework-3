package prometheus

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

func Run(address string, handle string) *http.Server {
	sm := http.NewServeMux()
	sm.Handle(handle, promhttp.Handler())
	srv := http.Server{Addr: address, Handler: sm}
	go func() {
		_ = srv.ListenAndServe()
	}()
	return &srv
}
