package producer

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"strconv"
)

func Send(producer sarama.SyncProducer, topic string, key int, v any) error {
	b, err := json.Marshal(v)
	if err != nil {
		return fmt.Errorf("json.Marshal: %w", err)
	}
	_, _, err = producer.SendMessage(&sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(strconv.Itoa(key)),
		Value: sarama.ByteEncoder(b),
	})
	if err != nil {
		return fmt.Errorf("can't send message: %w", err)
	}
	return nil
}
