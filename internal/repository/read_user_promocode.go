package repository

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) ReadUserPromoCode(ctx context.Context, ID int64) (models.UserPromoCode, error) {
	const query = `
		select id, userid, pid, used
		from user_promocodes
		where id = $1;
	`
	p := models.UserPromoCode{}
	err := r.pool.QueryRow(ctx, query, ID).Scan(
		&p.ID,
		&p.UserId,
		&p.Pid,
		&p.Used,
	)
	return p, err
}
