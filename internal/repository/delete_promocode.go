package repository

import "context"

func (r *repository) DeletePromoCode(ctx context.Context, ID int64) error {
	const query = `
		delete from promocodes
		where id = $1;
	`
	cmd, err := r.pool.Exec(ctx, query, ID)
	if err != nil {
		return err
	}
	if cmd.RowsAffected() == 0 {
		return ErrNotFound
	}
	return err
}
