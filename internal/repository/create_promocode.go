package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) CreatePromoCode(ctx context.Context, p models.PromoCode) (int64, error) {
	const query = `
		insert into promocodes (
			code,
			usage_count,
			used,
			deadline
		) VALUES (
			$1, $2, $3, $4
		) returning id
	`
	var ID int64
	err := r.pool.QueryRow(ctx, query,
		p.Code,
		p.UsageCount,
		0,
		p.Deadline,
	).Scan(&ID)
	return ID, err
}
