package repository

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) ReadPromoCode(ctx context.Context, ID int64) (models.PromoCode, error) {
	const query = `
		select id, code, usage_count, used, deadline
		from promocodes
		where id = $1;
	`
	p := models.PromoCode{}
	err := r.pool.QueryRow(ctx, query, ID).Scan(
		&p.ID,
		&p.Code,
		&p.UsageCount,
		&p.Used,
		&p.Deadline,
	)
	return models.PromoCode{}, err
}
