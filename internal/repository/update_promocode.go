package repository

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) UpdatePromoCode(ctx context.Context, p models.PromoCode) error {
	const query = `
		update promocodes 
		set code=$2, usage_count=$3, used=$4, deadline=$5
		where id = $1
	`
	cmd, err := r.pool.Exec(ctx, query,
		p.ID,
		p.Code,
		p.UsageCount,
		p.Used,
		p.Deadline,
	)
	if err != nil {
		return err
	}
	if cmd.RowsAffected() == 0 {
		return ErrNotFound
	}
	return err
}
