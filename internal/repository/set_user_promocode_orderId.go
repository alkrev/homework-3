package repository

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) SetUserPromoCodeOrderId(ctx context.Context, p models.UserPromoCode) error {
	const query = `
		update user_promocodes 
		set orderid=$3
		where userid = $1 AND pid = $2 AND NOT used
	`
	cmd, err := r.pool.Exec(ctx, query,
		p.UserId,
		p.Pid,
		p.OrderId,
	)
	if err != nil {
		return err
	}
	if cmd.RowsAffected() == 0 {
		return ErrNotFound

	}
	return err
}
