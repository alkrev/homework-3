package repository

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) GetActivePromoCodeByCode(ctx context.Context, code string) (models.PromoCode, error) {
	const query = `
		select id, code, usage_count, used, deadline
		from promocodes
		where code = $1 AND usage_count > used AND deadline > now();
	`
	p := models.PromoCode{}
	err := r.pool.QueryRow(ctx, query, code).Scan(
		&p.ID,
		&p.Code,
		&p.UsageCount,
		&p.Used,
		&p.Deadline,
	)
	return p, err
}
