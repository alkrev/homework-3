package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) CreatePayment(ctx context.Context, p models.Payment) (int64, error) {
	const query = `
		insert into payments (
			userid,
			orderid,
			status,
			"date"
		) VALUES (
			$1, $2, $3, $4
		) returning id
	`
	var ID int64
	err := r.pool.QueryRow(ctx, query,
		p.UserId,
		p.OrderId,
		p.Status,
		p.Date,
	).Scan(&ID)
	return ID, err
}
