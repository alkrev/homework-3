package repository

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) UpdateUserPromoCode(ctx context.Context, p models.UserPromoCode) error {
	const query = `
		update user_promocodes 
		set userid=$2, pid=$2, used=$4
		where id = $1
	`
	cmd, err := r.pool.Exec(ctx, query,
		p.ID,
		p.UserId,
		p.Pid,
		p.Used,
	)
	if err != nil {
		return err
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
	}
	return err
}
