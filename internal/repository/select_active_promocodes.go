package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) SelectActivePromoCodes(ctx context.Context) ([]models.PromoCode, error) {
	const query = `
	select id, code, usage_count, used, deadline
	from promocodes
	where usage_count > used AND deadline > now();
	`
	rows, err := r.pool.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var promoCodes []models.PromoCode
	for rows.Next() {
		var p models.PromoCode
		err := rows.Scan(
			&p.ID,
			&p.Code,
			&p.UsageCount,
			&p.Used,
			&p.Deadline,
		)
		if err != nil {
			return nil, err
		}
		promoCodes = append(promoCodes, p)
	}
	err = rows.Err()
	return promoCodes, err
}
