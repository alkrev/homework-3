package repository

import "context"

func (r *repository) IncPromoCodeUsage(ctx context.Context, ID int64) error {
	const query = `
		UPDATE promocodes
		SET used = used + 1
		where id = $1
	`
	cmd, err := r.pool.Exec(ctx, query, ID)
	if err != nil {
		return err
	}
	if cmd.RowsAffected() == 0 {
		return ErrNotFound

	}
	return err
}
