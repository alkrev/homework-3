package repository

import (
	"context"

	"gitlab.ozon.dev/alkrev/homework-3/internal/models"
)

func (r *repository) CreateUserPromoCode(ctx context.Context, up models.UserPromoCode) (int64, error) {
	const query = `
		insert into user_promocodes (
			userid,
			pid,
			used,
			orderid
		) VALUES (
			$1, $2, $3, $4
		) returning id
	`
	var ID int64
	err := r.pool.QueryRow(ctx, query,
		up.UserId,
		up.Pid,
		up.Used,
		0,
	).Scan(&ID)
	return ID, err
}
